library('shiny')
library('lubridate')
library('stringi')
library('shinythemes')
library('dplyr')
library('ggplot2')

load('./data/vertecar.rda')
load('./data/selectors.rda')
texts <- read.csv('./data/texts.csv', stringsAsFactors = F)

fluidPage(
 # shinythemes::themeSelector(),
  theme = shinytheme("flatly"), 
  tags$head(includeScript("ga.js"),
            tags$meta(name="google-site-verification", content="jKdZD2KyeWOMC9hdONvWq3XeUblJeT0JT6R8-9zC75Q"),
            tags$meta(name="description", content="Saņem atbildi uzreiz: verteR! ir auto vērtēšanas rīks, balstīts uz reāliem automašīnu cenu datiem par pēdējiem 3 gadiem."),
            tags$meta(name="keywords", content="auto cena, auto vērtējums, auto vertejums, novērtē auto, auto vērtēšana, novērtēt auto, auto, vertesana, vērtēšana")
            
  ),
  titlePanel("verteR! | auto vērtējums internetā"),
  
  mainPanel(width = 12,
            selectInput('make', 'Marka', choices = as.character(unique(selectors$make)), selected = 'Subaru', width = '100%'),
            uiOutput('model', label= 'Modelis', width = '100%'),
            sliderInput(inputId = 'year', label = 'Gads', min = 1980, max = lubridate::year(Sys.Date()), value = 2009, step = 1, sep = '', width = '100%'),
            radioButtons(inputId = 'fuel', label = 'Degviela',choices = c('Dīzelis','Benzīns','Hibrīds'), inline = T,width = '100%'),
            sliderInput(inputId = 'vol', label = 'Tilpums, l',min = 1, max = 6.0, value = 2.0, step = 0.1,sep = '',width = '100%'),
            sliderInput(inputId = 'mil', label = 'Nobraukums, t.k.',min = 0, max = 300, value = 100, step = 10,sep = '',width = '100%'),
            h2(textOutput('price')),
            actionButton(inputId = 'go', label = "Novērtēt"),
            #imageOutput(outputId = 'pic', width = '100%', height = '100%')
            
            tags$div(class="header", checked=NA,
                     tags$p(" "),
                     tags$a(href="http://www.epistatica.com", " by epistatica")
            )
  )
)