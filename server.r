  library('shiny')
  library('lubridate')
  library('stringi')
  library('shinythemes')
  library('dplyr')
  library('ggplot2')
  
  load('./data/vertecar.rda')
  load('./data/selectors.rda')
  texts <- read.csv('./data/texts.csv', stringsAsFactors = F)
  
shinyServer(function(input, output){ 
  
  output$model <- renderUI({
    selected_make <- input$make
    selectInput('model', 'Modelis', as.character(unique(selectors$model[selectors$make==selected_make])), selected = 'Impreza', width = '100%')
  })
  
  price <- eventReactive(input$go, {calcPrice()})
  output$price <- renderText({
    price()
  })
  
  pricePlot <- eventReactive(input$go, {calcAllPrices()})
  output$plt <- renderPlot({
    pricePlot()
    })
  
  calcPrice <- function() {
    #as.numeric(difftime(now(), cars$ddate))
    diffdays <- as.numeric(difftime('2016-12-27', now()))
    #diffdays <- 0
    age <- lubridate::year(Sys.Date()) - input$year
    fuel <- if(input$fuel=='Dīzelis'){'D'} else if (input$fuel=='Benzīns'){'P'} else {'H'}
    new_data <- list(make=input$make, model=input$model, diffdays = diffdays, age = age, vol=input$vol,
                     mil=input$mil, fuel=fuel, year=input$year)
    
    if (!is.null(new_data$model)) {#seems that model is not ready for the first prediction
      try({
        pred <- predict(model1, newdata = new_data)
        price <- sprintf("%1.0f",exp(pred))
        countof <- selectors$count_of[which(selectors$make==input$make & selectors$model==input$model)]
        texts$lv[1] %>%
          gsub(.,pattern='vPrice', replacement=price) %>%
          gsub(.,pattern='vCountOf', replacement=countof)
      })
    }
  }
  
  calcAllPrices <- function(){
    diffdays <- 0
    age <- lubridate::year(Sys.Date()) - input$year
    fuel <- if(input$fuel=='Dīzelis'){'D'} else if (input$fuel=='Benzīns'){'P'} else {'H'}
    
    #newdata <- list(make='Bmw', model='3', diffdays = 0, age = 10, vol=2, mil=100, fuel='D', year=2007)
    
    new_data <- list(make=input$make, model=input$model, diffdays = diffdays, age = age, vol=input$vol,
                      mil=input$mil, fuel=fuel, year=input$year)
    
    if (!is.null(new_data$model)) {#seems that model is not ready for the first prediction
      try({
       times <- 10 
       df <- data.frame(new_data)[rep(1,times),]
       df$age <- seq(min(df$age)-times/2,to = min(df$age)+times/2-1,by=1)
       df$y <- seq(year(Sys.time())-times/2,to = year(Sys.time())+times/2-1,by=1)
       df$diffdays <- seq(min(df$diffdays) + 360*(times/2), to = min(df$diffdays)-360*(times/2-1),by=-360)
       pred <- predict(model1, df)
       price <- sprintf("%1.0f",exp(pred))
       df <- data.frame(y=as.numeric(df$y),p=as.numeric(price))
       ggplot(df, aes(y, p)) + geom_line()
      })
    }
  }
  output$disclaimer <-  renderText(texts$lv[3])
})